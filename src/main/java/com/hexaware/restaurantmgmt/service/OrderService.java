package com.hexaware.restaurantmgmt.service;

import com.hexaware.restaurantmgmt.model.Order;

import java.util.Set;

public interface OrderService {

    public Order saveOrder(Order order);

    public Set<Order> fetchAllOrders();

    public Order fetchOrderById(long orderId);

    public void deleteOrderById(long orderId);
}
package com.hexaware.restaurantmgmt.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import static lombok.AccessLevel.NONE;

@Setter
@Getter()
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(of="orderLineItemId")
public class OrderLineItem {

    @JsonProperty("line_item_id")
    private int oderLineItemId;

    @JsonProperty("unit_price")
    private double unitPrice;

    @JsonProperty("quantity")
    private int quantity;

    @JsonProperty("dish")
    private Dish dish;

    @Getter(NONE)
    private Order order;
}
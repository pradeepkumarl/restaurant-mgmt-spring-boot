package com.hexaware.restaurantmgmt.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDate;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of = "orderId")
@Builder
@AllArgsConstructor
public class Order {

    @JsonProperty("id")
    private long orderId;

    @JsonProperty("order_date")
    private LocalDate orderDate;

    @JsonProperty("order_price")
    private double orderPrice;

    @JsonProperty("customer_name")
    private String customerName;

    @JsonProperty("line_items")
    private Set<OrderLineItem> orderLineItems;


}
package com.hexaware.restaurantmgmt.controller;

import com.hexaware.restaurantmgmt.model.Order;
import com.hexaware.restaurantmgmt.service.OrderService;
import com.hexaware.restaurantmgmt.service.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/hello-world")
    public String[] helloWorld(){
        return new String[]{"hello ", "world"};
    }

    @GetMapping
    public Set<Order> fetchAllOrders(){
     return this.orderService.fetchAllOrders();
    }

    @GetMapping("/{orderId}")
    public Order fetchOrderById(@PathVariable long orderId){
        return this.orderService.fetchOrderById(orderId);
    }

    @PostMapping
    public Order saveOrder(@RequestBody  Order order){
        return this.orderService.saveOrder(order);
    }

    @DeleteMapping("/{orderId}")
    public void deleteOrderById(@PathVariable("orderId") long id){
        this.orderService.deleteOrderById(id);
    }


}